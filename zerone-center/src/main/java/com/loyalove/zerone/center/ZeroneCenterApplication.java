package com.loyalove.zerone.center;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author loyalove
 */
@SpringBootApplication
@EnableEurekaServer
public class ZeroneCenterApplication {

  public static void main(String[] args) {
    SpringApplication.run(ZeroneCenterApplication.class, args);
  }
}
