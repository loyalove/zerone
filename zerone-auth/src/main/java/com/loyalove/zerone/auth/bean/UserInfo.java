package com.loyalove.zerone.auth.bean;


import lombok.Data;

/**
 * @author loyalove
 */
@Data
public class UserInfo {

  /**
   * 密码
   */
  private String password;
}
