package com.loyalove.zerone.auth.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author loyal
 */
@RestController
@RequestMapping("/auth")
public class AuthController {

  @RequestMapping("")
  public String index(){
    return "auth";
  }
}
