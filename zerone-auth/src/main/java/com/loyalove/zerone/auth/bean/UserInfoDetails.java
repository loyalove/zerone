package com.loyalove.zerone.auth.bean;

import java.util.Collection;
import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

public class UserInfoDetails implements UserDetails, CredentialsContainer {

  private UserInfo userInfo;
  private User user;

  public UserInfoDetails(UserInfo userInfo, User user) {
    this.userInfo = userInfo;
    this.user = user;
  }

  @Override
  public void eraseCredentials() {
    user.eraseCredentials();
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return user.getAuthorities();
  }

  @Override
  public String getPassword() {
    return user.getPassword();
  }

  @Override
  public String getUsername() {
    return user.getUsername();
  }

  @Override
  public boolean isAccountNonExpired() {
    return user.isAccountNonExpired();
  }

  @Override
  public boolean isAccountNonLocked() {
    return user.isAccountNonLocked();
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return user.isCredentialsNonExpired();
  }

  @Override
  public boolean isEnabled() {
    return user.isEnabled();
  }

  public UserInfo getUserInfo() {
    return userInfo;
  }
}
