package com.loyalove.zerone.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

/**
 * @author loyalove
 */
@SpringCloudApplication
public class ZeroneAuthApplication {
  public static void main(String[] args) {
    SpringApplication.run(ZeroneAuthApplication.class, args);
  }
}
