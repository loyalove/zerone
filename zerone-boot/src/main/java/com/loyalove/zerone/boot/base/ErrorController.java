package com.loyalove.zerone.boot.base;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

/**
 * 错误处理Controller
 *
 * @author loyalove
 * @date 2018/9/11 16:09
 */
@Controller
public class ErrorController extends BasicErrorController {

  public ErrorController(ServerProperties serverProperties) {
    super(new DefaultErrorAttributes(), serverProperties.getError());
  }

  @Override
  public ModelAndView errorHtml(HttpServletRequest request, HttpServletResponse response) {
    return super.errorHtml(request, response);
  }

  @Override
  public ResponseEntity<Map<String, Object>> error(HttpServletRequest request) {
    return super.error(request);
  }
}
