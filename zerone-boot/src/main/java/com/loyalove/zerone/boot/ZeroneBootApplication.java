package com.loyalove.zerone.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author loyalove
 */
@SpringBootApplication
public class ZeroneBootApplication {

  public static void main(String[] args) {
    SpringApplication.run(ZeroneBootApplication.class, args);
  }
}
