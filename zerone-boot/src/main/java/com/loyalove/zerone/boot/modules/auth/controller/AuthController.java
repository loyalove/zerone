package com.loyalove.zerone.boot.modules.auth.controller;

import com.loyalove.zerone.boot.base.BaseController;
import com.loyalove.zerone.common.entity.Result;
import com.loyalove.zerone.common.exception.BizException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 描述
 *
 * @author loyalove
 * @date 2018/9/11 16:59
 */
@RestController
@RequestMapping("/auth")
public class AuthController extends BaseController {

  @GetMapping
  public Result get(){
    throw new BizException("获取token失败");
//    return Result.success("获取token成功", "asd1asd2qqw2dqwd");
  }

  @PostMapping
  public Result post(){
    return Result.success("授权成功");
  }
}
