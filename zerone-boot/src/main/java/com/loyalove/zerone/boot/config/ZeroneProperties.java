package com.loyalove.zerone.boot.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = ZeroneProperties.PREFIX)
@Data
public class ZeroneProperties {
  protected static final String PREFIX = "zerone";

  private Boolean swagger = false;
}
