package com.loyalove.zerone.boot.common.utils;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.loyalove.zerone.common.exception.BizException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

/**
 * Title: ValidatorUtil.java Description: 对象验证器 Company: ysh
 *
 * @author: sailuo@yiji.com
 * @date: 2017-08-25 16:27
 */
public class ValidatorUtil {

  private ValidatorUtil() {
  }

  private static final String conjunction = ",";

  /**
   * 验证对象是否符合规则，返回第一条不符合规则的消息
   */
  public static <T> String validate(T object) {
    Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
    Set<ConstraintViolation<T>> violations = validator.validate(object);
    for (ConstraintViolation<T> violation : violations) {
      return violation.getMessage();
    }
    return null;
  }

  /**
   * 验证对象是否符合规则，返回所有不符合规则的消息
   */
  public static <T> String validateAll(T object) {
    Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
    final Set<ConstraintViolation<T>> violations = validator.validate(object);
    List<String> msgs = new ArrayList<>();
    for (ConstraintViolation<T> violation : violations) {
      msgs.add(violation.getMessage());
    }
    return StrUtil.join(conjunction, msgs.toArray());
  }

  /**
   * 验证对象是否符合规则，返回所有不符合规则的消息
   */
  public static String getValidateMsg(BindingResult result) {

    List<String> msgs = new ArrayList<>();
    for (ObjectError objectError : result.getAllErrors()) {
      msgs.add(objectError.getDefaultMessage());
    }
    return StrUtil.join(conjunction, msgs.toArray());
  }

  /**
   * 验证对象是否符合规则，返回所有不符合规则的消息
   */
  public static String getValidateMsg(BindException result) {

    List<String> msgs = new ArrayList<>();
    for (ObjectError objectError : result.getAllErrors()) {
      msgs.add(objectError.getDefaultMessage());
    }
    return StrUtil.join(conjunction, msgs.toArray());
  }

  /**
   * 验证对象是否符合规则，抛出业务异常
   */
  public static <T> void validateThrow(T object) {
    Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
    Set<ConstraintViolation<T>> violations = validator.validate(object);
    for (ConstraintViolation<T> violation : violations) {
      throw new BizException(violation.getMessage());
    }
  }

  /**
   * 验证对象是否符合规则，抛出业务异常
   */
  public static <T> void validateAllThrow(T object) {
    Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
    final Set<ConstraintViolation<T>> violations = validator.validate(object);

    if (CollectionUtil.isEmpty(violations)) {
      return;
    }

    List<String> msgs = new ArrayList<>();
    for (ConstraintViolation<T> violation : violations) {
      msgs.add(violation.getMessage());
    }
    throw new BizException(StrUtil.join(conjunction, msgs.toArray()));
  }

}
