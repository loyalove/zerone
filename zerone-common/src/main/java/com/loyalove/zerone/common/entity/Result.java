package com.loyalove.zerone.common.entity;

import com.loyalove.zerone.common.enums.ResultStatus;
import java.io.Serializable;
import lombok.Builder;
import lombok.Data;

/**
 * 响应结果
 *
 * @author loyalove
 * @date 2018/9/7 15:00
 */
@Data
@Builder
public class Result implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * 状态码
   */
  private int status;

  /**
   * 消息
   */
  private String message;

  /**
   * 数据
   */
  private Object data;

  /**
   * 数据总数
   */
  private long count;


  public static Result success(String message, Object data, long count) {
    return new Result(ResultStatus.STATUS_200.status(), message, data, count);
  }

  public static Result success(Object data, long count) {
    return success(ResultStatus.STATUS_200.message(), data, count);
  }

  public static Result success(Object data, Integer count) {
    return success(ResultStatus.STATUS_200.message(), data, Integer.toUnsignedLong(count));
  }

  public static Result success(Object data) {
    return success(data, 0);
  }

  public static Result success(String message, Object data) {
    return success(message, data, 0);
  }

  public static Result success(String message) {
    return success(message, null, 0);
  }

  public static Result fail(String message) {
    return new Result(ResultStatus.STATUS_400.status(), message, null, 0);
  }

  public static Result error(String message) {
    return new Result(ResultStatus.STATUS_500.status(), message, null, 0);
  }

}
