package com.loyalove.zerone.common.exception;

/**
 * 服务异常
 *
 * @author loyalove
 * @date 2018/9/7 11:22
 */
public class ServiceException extends BaseException {

  public ServiceException(String message) {
    super(message);
  }

  public ServiceException(String message, Throwable cause) {
    super(message, cause);
  }
}
