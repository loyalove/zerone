package com.loyalove.zerone.common.exception;

/**
 * 业务异常
 *
 * @author loyalove
 * @date 2018/9/7 11:22
 */
public class BizException extends BaseException {

  public BizException(String message) {
    super(message);
  }

  public BizException(String message, Throwable cause) {
    super(message, cause);
  }
}
