package com.loyalove.zerone.common.enums;

/**
 * 结果状态
 *
 * @author loyalove
 * @date 2018/9/7 15:35
 */
public enum ResultStatus {

  /**
   * 处理成功
   */
  STATUS_200(200, "处理成功"),

  /**
   * 请求错误
   */
  STATUS_400(400, "请求错误"),

  /**
   * 服务器异常
   */
  STATUS_500(500, "服务器异常"),
  ;

  /**
   * 状态码
   */
  private int status;

  /**
   * 消息
   */
  private String message;

  /**
   * 构造方法
   * @param status 状态码
   * @param message 消息
   */
  ResultStatus(int status, String message) {
    this.status = status;
    this.message = message;
  }

  public int status() {
    return status;
  }

  public String message() {
    return message;
  }
}
