package com.loyalove.zerone.common.exception;

/**
 * Base异常类
 *
 * @author loyalove
 * @date 2018/9/7 11:16
 */
public class BaseException extends RuntimeException {

  public BaseException(String message) {
    super(message);
  }

  public BaseException(String message, Throwable cause) {
    super(message, cause);
  }
}