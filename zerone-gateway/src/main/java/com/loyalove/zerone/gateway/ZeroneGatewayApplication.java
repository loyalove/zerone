package com.loyalove.zerone.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author loyalove
 */
@SpringBootApplication
public class ZeroneGatewayApplication{

  public static void main(String[] args) {
    SpringApplication.run(ZeroneGatewayApplication.class, args);
  }

}