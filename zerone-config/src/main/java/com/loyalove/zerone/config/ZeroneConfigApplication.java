package com.loyalove.zerone.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * @author loyalove
 */
@SpringBootApplication
@EnableConfigServer
public class ZeroneConfigApplication {

  public static void main(String[] args) {
    SpringApplication.run(ZeroneConfigApplication.class, args);
  }
}
