package com.loyalove.zerone.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author loyalove
 */
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class ZeroneTestApplication {

  public static void main(String[] args) {
    SpringApplication.run(ZeroneTestApplication.class, args);
  }
}
