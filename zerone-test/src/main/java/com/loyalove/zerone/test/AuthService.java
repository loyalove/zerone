package com.loyalove.zerone.test;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient("zerone-auth")
public interface AuthService {

  /**
   * 权限校验
   * @return
   */
  @PostMapping("/auth")
  String auth();

}
