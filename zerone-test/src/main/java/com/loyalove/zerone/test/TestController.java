package com.loyalove.zerone.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

  @Autowired
  AuthService authService;

  @GetMapping("/")
  public String test(){
    return authService.auth();
  }
}
